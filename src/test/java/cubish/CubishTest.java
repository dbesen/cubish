// vim: expandtab ts=8 shiftwidth=8
package cubish;

import java.util.List;
import java.util.Set;
import java.util.Collection;

import cubish.Cubish;

import junit.framework.TestCase;

import static cubish.CubishAsserts.*;

public class CubishTest extends TestCase {

        public static final int joeDarkMagnets = 1585;
        public static final int joeLightMagnets = 8902;

        public void testManually() {
                int numMagnets = joeDarkMagnets;
                System.out.println("1 cube: " + Cubish.getBestCubish(numMagnets));

                List<Cubish> bestCubishGroup = Cubish.getBestCubishGroup(numMagnets, 2);
                System.out.println("2 cubes: " + bestCubishGroup.get(0) + ", " + bestCubishGroup.get(1));

                bestCubishGroup = Cubish.getBestCubishGroup(numMagnets, 3);
                System.out.println("3 cubes: " + bestCubishGroup.get(0) + ", " + bestCubishGroup.get(1) + ", " + bestCubishGroup.get(2));

                // too slow!
                //bestCubishGroup = Cubish.getBestCubishGroup(numMagnets, 4);
                //System.out.println("4 cubes: " + bestCubishGroup.get(0) + ", " + bestCubishGroup.get(1) + ", " + bestCubishGroup.get(2) + ", " + bestCubishGroup.get(3));
        }

        public void testOne() {
                List<Cubish> bestCubishGroup = Cubish.getBestCubishGroup(3 * 3 * 3, 1);
                assertEquals(1, bestCubishGroup.size());
                assertCubishEquals(3, 3, 3, bestCubishGroup.get(0));
        }

        public void testTwo() {
                List<Cubish> bestCubishGroup = Cubish.getBestCubishGroup(2 * 2 * 2 * 2, 2);
                assertEquals(2, bestCubishGroup.size());
                assertCubishEquals(2, 2, 2, bestCubishGroup.get(0));
                assertCubishEquals(2, 2, 2, bestCubishGroup.get(1));
        }

        public void testThree() {
                List<Cubish> bestCubishGroup = Cubish.getBestCubishGroup(3 * 3 * 3 * 3, 3);
                assertEquals(3, bestCubishGroup.size());
                assertCubishEquals(3, 3, 3, bestCubishGroup.get(0));
                assertCubishEquals(3, 3, 3, bestCubishGroup.get(1));
                assertCubishEquals(3, 3, 3, bestCubishGroup.get(2));
        }

        public void testFour() {
                List<Cubish> bestCubishGroup = Cubish.getBestCubishGroup(4 * 4 * 4 * 4, 4);
                assertEquals(4, bestCubishGroup.size());
                assertCubishEquals(4, 4, 4, bestCubishGroup.get(0));
                assertCubishEquals(4, 4, 4, bestCubishGroup.get(1));
                assertCubishEquals(4, 4, 4, bestCubishGroup.get(2));
                assertCubishEquals(4, 4, 4, bestCubishGroup.get(3));
        }

        public void testThings() {
                assertCubishEquals(47, 23, 3, Cubish.getBestCubish(3243));

                List<Cubish> bestCubishGroup = Cubish.getBestTwoCubishes(3243);
                assertEquals(2, bestCubishGroup.size());
                assertCubishContains(17, 11, 9, bestCubishGroup);
                assertCubishContains(13, 12, 10, bestCubishGroup);
        }

        public void testGetCubishSizes() {
                assertCubishContains(1, 1, 1, Cubish.getCubishSizes(1));
                assertCubishContains(2, 1, 1, Cubish.getCubishSizes(2));
                assertCubishContains(10, 10, 10, Cubish.getCubishSizes(10*10*10));
                assertCubishContains(11, 10, 10, Cubish.getCubishSizes(10*10*11));
                assertCubishContains(150, 1, 1, Cubish.getCubishSizes(150));
                assertCubishContains(3, 2, 1, Cubish.getCubishSizes(6));
                assertCubishContains(6, 1, 1, Cubish.getCubishSizes(6));
        }

        public void testGetBestCubish() {
                assertCubishEquals(10, 10, 10, Cubish.getBestCubish(10*10*10));
                assertCubishEquals(3, 2, 1, Cubish.getBestCubish(6));
        }
}

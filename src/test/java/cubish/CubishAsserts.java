package cubish;

import cubish.Cubish;
import java.util.Collection;

import static org.junit.Assert.*;

public class CubishAsserts {
        public static void assertCubishEquals(int x, int y, int z, Cubish c) {
                Cubish newCubish = new Cubish(x, y, z);
                assertEquals(newCubish, c);
        }

        public static void assertCubishContains(int x, int y, int z, CubeGroup l) {
                assertCubishContains(x, y, z, l.getCubeList());
        }

        public static void assertCubishContains(int x, int y, int z, Collection<Cubish> l) {
                Cubish newCubish = new Cubish(x, y, z);
                for(Cubish c : l) {
                        if(c.equals(newCubish)) return;
                }
                fail("Not found: " + newCubish);
        }
}

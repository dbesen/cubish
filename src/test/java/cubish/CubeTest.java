package cubish;

import org.junit.Test;
import static org.junit.Assert.*;

import static cubish.CubishAsserts.*;

public class CubeTest {
        @Test
        public void testManually() {
                printResult(CubishTest.joeDarkMagnets);
                printResult(CubishTest.joeLightMagnets);
        }

        private void printResult(int numMagnets) {
                CubeGroup bestCubeGroup = Cubes.getBestCubeGroup(numMagnets, 2);
                System.out.println("" + numMagnets + " magnets: " + bestCubeGroup + ": " + bestCubeGroup.remaining(numMagnets) + " remaining");
        }

        @Test
        public void testLargestThatFits() {
                assertCubishEquals(1, 1, 1, Cubes.getLargestCubeThatFits(1));
                assertCubishEquals(1, 1, 1, Cubes.getLargestCubeThatFits(2));
                assertCubishEquals(2, 2, 2, Cubes.getLargestCubeThatFits(2 * 2 * 2));
                assertCubishEquals(2, 2, 2, Cubes.getLargestCubeThatFits(2 * 2 * 2 + 1));
                assertCubishEquals(200, 200, 200, Cubes.getLargestCubeThatFits(200 * 200 * 200));
                assertCubishEquals(200, 200, 200, Cubes.getLargestCubeThatFits(200 * 200 * 200 + 1));
        }

        @Test
        public void testOnlyCubesOneMax() {
                CubeGroup bestCubeGroup = Cubes.getBestCubeGroup(3 * 3 * 3, 1);
                assertEquals(1, bestCubeGroup.size());
                assertCubishContains(3, 3, 3, bestCubeGroup);
                assertEquals(0, bestCubeGroup.remaining(3 * 3 * 3));
        }

        @Test
        public void testOnlyCubesOneTwoMax() {
                CubeGroup bestCubeGroup = Cubes.getBestCubeGroup(3 * 3 * 3, 2);
                assertEquals(1, bestCubeGroup.size());
                assertCubishContains(3, 3, 3, bestCubeGroup);
        }

        @Test
        public void testOnlyCubesOneThreeMax() {
                CubeGroup bestCubeGroup = Cubes.getBestCubeGroup(3 * 3 * 3, 3);
                assertEquals(1, bestCubeGroup.size());
                assertCubishContains(3, 3, 3, bestCubeGroup);
        }

        @Test
        public void testOnlyCubesTwoSame() {
                CubeGroup bestCubeGroup = Cubes.getBestCubeGroup(3 * 3 * 3 * 2, 2);
                assertEquals(2, bestCubeGroup.size());
                assertCubishEquals(3, 3, 3, bestCubeGroup.get(0));
                assertCubishEquals(3, 3, 3, bestCubeGroup.get(1));
        }

        @Test
        public void testOnlyCubesTwoDifferent() {
                CubeGroup bestCubeGroup = Cubes.getBestCubeGroup((2 * 2 * 2) + (10 * 10 * 10), 2);
                assertEquals(2, bestCubeGroup.size());
                assertCubishContains(2, 2, 2, bestCubeGroup);
                assertCubishContains(10, 10, 10, bestCubeGroup);
        }
}

package cubish;

import java.util.List;
import java.util.ArrayList;

public class CubeGroup {

        private List<Cubish> cubishes = new ArrayList<Cubish>();

        public Cubish get(int index) {
                return cubishes.get(index);
        }

        public List<Cubish> getCubeList() {
                return cubishes;
        }

        public int size() {
                return cubishes.size();
        }

        public int remaining(int numMagnets) {
                int total = 0;
                for(Cubish i : cubishes) {
                        total += i.numMagnets();
                }
                return numMagnets - total;
        }

        public void add(Cubish a) {
                cubishes.add(a);
        }

        public String toString() {
                String ret = ""; // yeah yeah stringbuilder
                for(Cubish i : cubishes) {
                        ret += i + ", ";
                }
                return ret;
        }
}

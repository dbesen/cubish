package cubish;

import java.util.HashMap;

class Cubes {
        public static CubeGroup getBestCubeGroup(int numMagnets, int maxCubes) {
                int lowestRemaining = Integer.MAX_VALUE;
                CubeGroup bestGroup = null;

                for(int i=1;i<=maxCubes;i++) {
                        CubeGroup curr = getBestCubeGroupOnly(numMagnets, i);
                        int rem = curr.remaining(numMagnets);
                        if(rem < lowestRemaining) {
                                lowestRemaining = rem;
                                bestGroup = curr;
                        }
                }
                return bestGroup;
        }

        /**
         * Return the best cubish group made from exactly numCubes cubes.
         */
        public static CubeGroup getBestCubeGroupOnly(int numMagnets, int numCubes) {
                if(numCubes == 1) {
                        Cubish c = getLargestCubeThatFits(numMagnets);
                        CubeGroup g = new CubeGroup();
                        g.add(c);
                        return g;
                }
                int lowestRemaining = Integer.MAX_VALUE;
                CubeGroup bestGroup = null;
                for(int i=1;i<(numMagnets/numCubes)+1;i++) {
                        Cubish c = getLargestCubeThatFits(i);
                        CubeGroup otherCubes = getBestCubeGroupOnly(numMagnets - i, numCubes - 1);
                        otherCubes.add(c);
                        int rem = otherCubes.remaining(numMagnets);
                        if(rem < lowestRemaining) { // <=?
                                lowestRemaining = rem;
                                bestGroup = otherCubes;
                        }
                }
                return bestGroup;
        }

        public static Cubish getLargestCubeThatFits(int numMagnets) {
                for(int i=0;;i++) {
                        int c = i*i*i;
                        if(c > numMagnets) {
                                return new Cubish(i-1, i-1, i-1);
                        }
                }
        }
}

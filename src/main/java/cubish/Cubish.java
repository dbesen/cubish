// vim: expandtab ts=8 shiftwidth=8
package cubish;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.Map;

public class Cubish {
        @Override
        public int hashCode() {
                final int prime = 31;
                int result = 1;
                result = prime * result + x;
                result = prime * result + y;
                result = prime * result + z;
                return result;
        }

        @Override
        public boolean equals(Object obj) {
                if (this == obj)
                        return true;
                if (obj == null)
                        return false;
                if (getClass() != obj.getClass())
                        return false;
                Cubish other = (Cubish) obj;
                if (x != other.x)
                        return false;
                if (y != other.y)
                        return false;
                if (z != other.z)
                        return false;
                return true;
        }

        private final int x, y, z;

        // Guarantees that x >= y >= z.
        public Cubish(int x, int y, int z) {
                if(x < y || x < z || y < z) {
                        throw new IllegalArgumentException();
                }
                this.x = x;
                this.y = y;
                this.z = z;
        }

        public int getX() {
                return x;
        }

        public int getY() {
                return y;
        }

        public int getZ() {
                return z;
        }

        public String toString() {
                return "" + x + " x " + y + " x " + z;
        }

        /**
         * Return the list of all cubish shapes you can make from numMagnets magnets.
         * @param numMagnets
         */
        static Set<Cubish> getCubishSizes(int numMagnets) {
                Set<Cubish> list = new HashSet<Cubish>();
                List<Integer> factors = factor(numMagnets);
                for(int i : factors) {
                        for(int j : factors) {
                                if(j > i) break;
                                for(int k : factors) {
                                        if(k > j) break;
                                        if(i*j*k == numMagnets) {
                                                list.add(new Cubish(i, j, k));
                                        }
                                }
                        }
                }
                return list;
        }

        private static List<Integer> factor(int num) {
                List<Integer> factors = new ArrayList<Integer>();
                for(int i=1;i<=Math.sqrt(num);i++) {
                        if(num % i == 0) {
                                factors.add(i);
                                int other = num/i;
                                if(other != i)
                                        factors.add(other);
                        }
                }
                Collections.sort(factors);
                return factors;
        }

        // This method is memoized to make getBestCubishGroup faster.
        private static Map<Integer, Cubish> memoizeCache = new HashMap<Integer, Cubish>();
        public static Cubish getBestCubish(int i) {
                Cubish cached = memoizeCache.get(i);
                if(cached != null) return cached;

                Set<Cubish> candidates = getCubishSizes(i);
                Cubish bestCubish = null;

                for(Cubish c : candidates) {
                        bestCubish = getBetterCubish(bestCubish, c);
                }

                memoizeCache.put(i, bestCubish);
                return bestCubish;
        }

        public boolean isBetterThanOrEqualTo(Cubish other) {
                if(other == null) return true;
                if(z > other.z) return true;
                if(z < other.z) return false;
                if(y > other.y) return true;
                if(y < other.y) return false;
                if(x > other.x) return true;
                if(x < other.x) return false;
                return true;
        }

        public static List<Cubish> getBestCubishGroup(int numMagnets, int numCubes) {
                if(numCubes == 1) {
                        List<Cubish> toReturn = new ArrayList<Cubish>();
                        toReturn.add(getBestCubish(numMagnets));
                        return toReturn;
                }
                Cubish bestWorstCubish = null;
                List<Cubish> bestCubishGroup = null;
                for(int i=1;i<(numMagnets/numCubes)+1;i++) {
                        List<Cubish> toTest = new ArrayList<Cubish>();
                        int remaining = numMagnets - i;
                        toTest = getBestCubishGroup(remaining, numCubes - 1);

                        Cubish bestCubishI = getBestCubish(i);
                        toTest.add(bestCubishI);

                        Cubish worstCubish = getWorstCubish(toTest);
                        if(worstCubish.isBetterThanOrEqualTo(bestWorstCubish)) {
                                bestWorstCubish = worstCubish;
                                bestCubishGroup = toTest;
                        }

                }
                return bestCubishGroup;
        }

        public static List<Cubish> getBestTwoCubishes(int numMagnets) {
                return getBestCubishGroup(numMagnets, 2);
        }

        public static Cubish getBetterCubish(Cubish a, Cubish b) {
                if(a == null) return b;
                if(a.isBetterThanOrEqualTo(b)) return a;
                return b;
        }

        public static Cubish getWorseCubish(Cubish a, Cubish b) {
                if(a == null) return b;
                if(a.isBetterThanOrEqualTo(b)) return b;
                return a;
        }

        public static Cubish getWorstCubish(List<Cubish> cubishes) {
                Cubish worst = null;
                for(Cubish i : cubishes) {
                        if(worst == null) worst = i;
                        else if(worst.isBetterThanOrEqualTo(i)) worst = i;
                }
                return worst;
        }

        public int numMagnets() {
                return x * y * z;
        }
}
